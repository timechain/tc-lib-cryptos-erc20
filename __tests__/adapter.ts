import { BigNumber } from 'bignumber.js'
import { EthereumAdapter } from 'tc-lib-cryptos-ethereum'

import { ERC20Wallet } from '../src/models/wallet'
import { ERC20Adapter, ContractsAddresses } from '../src/index'

const TEST_ADDRESS = '0x1c2e996e31fe864444abe4700371acbfa2637a63'

async function prepareAdapter (address: string): Promise<ERC20Adapter> {
  const adapter = new ERC20Adapter({
    address: address,
    ethereumAdapter: EthereumAdapter
  })
  await adapter.waitForInit()
  return adapter
}

function environmentWallet (): ERC20Wallet {
  return {
    address: process.env.WALLET_ADDRESS!,
    privateKey: process.env.WALLET_PRIVATE_KEY!,
    publicKey: '',
    crypto: 'ETH'
  }
}

test('Get contract info', async () => {
  const adapter = await prepareAdapter(ContractsAddresses.Tronix)
  expect(adapter.name).toBe('Tronix')
  expect(adapter.crypto).toBe('TRX')
  expect(adapter.decimals).toBe(6)
  expect(adapter.contractAddress).toBe(ContractsAddresses.Tronix)
})

test('Import wallet', async () => {
  function testWallet (wallet: ERC20Wallet) {
    expect(EthereumAdapter.validateAddress(wallet.address, null)).toEqual(true)
    expect(wallet.address).toEqual('0xe9AA8a2eAd4A61d602232F8A3D1dDF6CB6a209c9')
  }

  // Seed
  let wallet = await EthereumAdapter.importWalletWithSeed('kingdom rabbit inject install bottom own machine spend result coffee hip off')
  testWallet(wallet)

  // Private key
  wallet = await EthereumAdapter.importWallet({ privateKey: '0x54774c86cfb8474708163deb7613e30ffc37a035a553f6e76fffe994006dc1cb' })
  testWallet(wallet)
})

test('Get balance', async () => {
  const adapter = await prepareAdapter(ContractsAddresses.Tronix)
  const balance = await adapter.getBalance({
    address: TEST_ADDRESS
  })
  expect(balance).toBeDefined()
})

test('Get transactions', async () => {
  const adapter = await prepareAdapter(ContractsAddresses.Tronix)
  const transactions = await adapter.getTransactions({
    address: TEST_ADDRESS
  })
  expect(transactions.length).toBeGreaterThan(0)
})
