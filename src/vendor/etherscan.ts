import axios from 'axios'
import * as ethers from 'ethers'

import { buildUrl } from '../utils/url'
import { ERC20Contract } from '../models/erc20-contract'
import { Transaction } from 'tc-lib-cryptos/dist/models'
import { EtherscanTransaction } from 'tc-lib-cryptos-ethereum/dist/vendor/etherscan'

const ETHERSCAN_BASE_URL = 'http://api.etherscan.io/api'

interface EtherscanResponse<T> {
  status: string
  message: string
  result: T
}

type GetAbiResponse = EtherscanResponse<string>
type GetERC20TransactionsResponse = EtherscanResponse<EtherscanTransaction[]>

export type ABI = any

export async function getContractABI (address: string): Promise<ABI> {
  const result = (await axios.get<GetAbiResponse>(ETHERSCAN_BASE_URL, {
    params: {
      module: 'contract',
      action: 'getabi',
      address: address
    }
  })).data.result
  return JSON.parse(result)
}

export async function getERC20Transactions (address: string, smartContractAddress?: string): Promise<EtherscanTransaction[]> {
  const result = (await axios.get<GetERC20TransactionsResponse>(ETHERSCAN_BASE_URL, {
    params: {
      module: 'account',
      action: 'tokentx',
      address: address
    }
  })).data.result

  if (smartContractAddress) {
    return result.filter(value => value.contractAddress.toLowerCase() === smartContractAddress.toLowerCase())
  } else {
    return result
  }
}
