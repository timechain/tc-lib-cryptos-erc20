import * as Web3 from 'web3'
import * as ethers from 'ethers'
import { BigNumber } from 'bignumber.js'
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models'
import { EthereumAdapterClass } from 'tc-lib-cryptos-ethereum'
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet'
import { ethersBigNumberToBigNumber, toEthersAmount, ethersWallet } from 'tc-lib-cryptos-ethereum/dist/vendor/ethers'

import { ERC20Wallet } from './models/wallet'
import { ERC20Contract } from './models/erc20-contract'
import { AsyncObject, WaitForReady } from './utils/wait-for-ready'
import * as contractsAddresses from './constants/contracts-addresses'
import { getContractABI, getERC20Transactions, ABI } from './vendor/etherscan'
import { toTransaction } from 'tc-lib-cryptos-ethereum/dist/vendor/etherscan'

// There is no fixed gas amount use so this is an average
const AVERAGE_GAS_USED = 75000
const GAS_LIMIT_FOR_TRANSACTION = 150000

/**
 * Non exhaustive list of ERC20 smart contracts addresses
 */
export const ContractsAddresses = contractsAddresses

export class ERC20Adapter extends AsyncObject implements WalletAdapter<ERC20Wallet> {
  crypto: string

  name: string = ''

  decimals: number

  private _contractAddress: string
  private _contract: ERC20Contract
  private _contractABI: ABI

  get contract (): ERC20Contract {
    return this._contract
  }

  get contractAddress (): string {
    return this._contractAddress
  }

  constructor (private config: {
    /**
     * The smart contract address
     */
    address: string,
    /**
     * The Ethereum adapter to use to perform the ERC20 operations
     */
    ethereumAdapter: EthereumAdapterClass
  }) {
    super()
    this._contractAddress = config.address
    this.setupContract().then()
  }

  async generateRandomWallet (): Promise<{
    wallet: ERC20Wallet,
    seed: string
  }> {
    return this.config.ethereumAdapter.generateRandomWallet()
  }

  async importWalletWithSeed (seed: string): Promise<ERC20Wallet> {
    return this.config.ethereumAdapter.importWalletWithSeed(seed)
  }

  async importWallet (info: {
    publicKey?: string;
    privateKey?: string;
    address?: string;
  }): Promise<ERC20Wallet> {
    return this.config.ethereumAdapter.importWallet(info)
  }

  @WaitForReady()
  async getBalance (wallet: Partial<Wallet>): Promise<BigNumber> {
    return ethersBigNumberToBigNumber(await this.contract.balanceOf(wallet.address!))
  }

  @WaitForReady()
  async sendTransaction (options: SendTransactionOptions<ERC20Wallet>): Promise<Transaction> {
    const ethWallet = ethersWallet(options.wallet.privateKey, this.config.ethereumAdapter.provider)

    const sendOptions: any = {
      gasLimit: GAS_LIMIT_FOR_TRANSACTION,
      gasPrice: await this.config.ethereumAdapter.provider.getGasPrice()
    }
    const contract = this.contractFromWallet(options.wallet)
    const transaction = await contract.transfer(options.recipientAddress, toEthersAmount(options.amount), sendOptions)

    return {
      hash: transaction.hash,
      date: new Date(),
      fromAddress: transaction.from,
      // Use the recipient address instead of transaction.to
      // to have the actual recipient address instead of the smart contract address
      toAddress: options.recipientAddress,
      amount: options.amount,
      crypto: this.crypto
    }
  }

  @WaitForReady()
  async getTransactionFees (params: SendTransactionOptions<ERC20Wallet>): Promise<BigNumber> {
    const ethWallet = ethersWallet(params.wallet.privateKey, this.config.ethereumAdapter.provider)

    const gasUsed = AVERAGE_GAS_USED
    const gasPrice = ethersBigNumberToBigNumber(await this.config.ethereumAdapter.provider.getGasPrice())
    return gasPrice.multipliedBy(gasUsed)
  }

  @WaitForReady()
  async getTransactions (wallet: Partial<Wallet>): Promise<Transaction[]> {
    if (!wallet.address) throw new Error('The provided wallet must have an address')

    const transactions: Transaction[] = []
    const etherscanTransactions = await getERC20Transactions(wallet.address!, this.config.address)

    for (const transaction of etherscanTransactions) {
      transactions.push(toTransaction(transaction, this.crypto))
    }
    return transactions
  }

  validateAddress (address: string, options: object | null): boolean {
    return this.config.ethereumAdapter.validateAddress(address, options)
  }

  /**
   * Creates an ERC20 smart contract object to be able to perform transactions on behalf on the wallet
   * @param wallet The wallet that will perform transactions
   */
  private contractFromWallet (wallet: ERC20Wallet): ERC20Contract {
    const ethWallet = ethersWallet(wallet.privateKey, this.config.ethereumAdapter.provider)
    return new ethers.Contract(this.config.address, this._contractABI, ethWallet)
  }

  private async setupContract () {
    this._contractABI = await getContractABI(this.config.address)
    this._contract = new ethers.Contract(this.config.address, this._contractABI, this.config.ethereumAdapter.provider)
    this.crypto = await this.contract.symbol()
    this.name = await this.contract.name()
    this.decimals = ethersBigNumberToBigNumber(await this.contract.decimals()).toNumber()
    this.setReady()
  }
}
