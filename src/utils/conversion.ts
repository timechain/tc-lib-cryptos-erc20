import * as ethers from 'ethers'
import { BigNumber } from 'bignumber.js'

export function gweiToWei (gwei: BigNumber): BigNumber {
  return gwei.multipliedBy(1000000000)
}
