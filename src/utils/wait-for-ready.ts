type WaitForInitCallback = () => void

/**
 * Wait for init annotation.
 * Blocks the decorated function until the object is ready to use.
 * Will only work with async functions.
 */
export function WaitForReady () {
  return function (target: AsyncObject, name: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value
    descriptor.value = function () {
      const originalArguments = arguments
      return new Promise(async (res, rej) => {
        await this.waitForInit()
        const result = originalMethod.apply(this, originalArguments)
        res(result)
      })
    }
    return descriptor
  }
}

export class AsyncObject {
  private _isInitialized = false
  private isInitCallbacks: WaitForInitCallback[] = []

  get isInitialized (): boolean {
    return this._isInitialized
  }

  async waitForInit (): Promise<void> {
    if (this.isInitialized) return

    return new Promise<void>(async (res, rej) => {
      if (!this.isInitCallbacks) this.isInitCallbacks = []
      this.isInitCallbacks.push(() => {
        res()
      })
    })
  }

  protected setReady () {
    this._isInitialized = true
    for (const callback of this.isInitCallbacks) callback()
    this.isInitCallbacks = []
  }
}
