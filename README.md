# Ethereum adapter library

ERC20 adapter for [Timechain Lib Cryptos](https://bitbucket.org/timechain/tc-lib-cryptos).

## Unit tests

In a terminal, run:

```sh
npm run test
```

To be able to perform transactions, you must set some environment variables.

```bash
# The address of the wallet to send transactions
export WALLET_ADDRESS="0x8dBD07D18a5a92e90c1ce57E08e1FD6250B2525a"

# The private of the wallet to send transactions 
export WALLET_PRIVATE_KEY="0x8dBD07D18a5a92e90c1ce57E08e1FD6250B2525a"

# The recipient address of test transactions
export RECIPIENT_ADDRESS="0x27967db2D1EC2012346164D24c869119564A9F27"

# Set no if you don't want to sent transactions in the tests
export PERFORM_TRANSACTIONS=true

```