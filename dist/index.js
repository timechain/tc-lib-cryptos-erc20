"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ethers = require("ethers");
var ethers_1 = require("tc-lib-cryptos-ethereum/dist/vendor/ethers");
var wait_for_ready_1 = require("./utils/wait-for-ready");
var contractsAddresses = require("./constants/contracts-addresses");
var etherscan_1 = require("./vendor/etherscan");
var etherscan_2 = require("tc-lib-cryptos-ethereum/dist/vendor/etherscan");
// There is no fixed gas amount use so this is an average
var AVERAGE_GAS_USED = 75000;
var GAS_LIMIT_FOR_TRANSACTION = 150000;
/**
 * Non exhaustive list of ERC20 smart contracts addresses
 */
exports.ContractsAddresses = contractsAddresses;
var ERC20Adapter = /** @class */ (function (_super) {
    __extends(ERC20Adapter, _super);
    function ERC20Adapter(config) {
        var _this = _super.call(this) || this;
        _this.config = config;
        _this.name = '';
        _this._contractAddress = config.address;
        _this.setupContract().then();
        return _this;
    }
    Object.defineProperty(ERC20Adapter.prototype, "contract", {
        get: function () {
            return this._contract;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ERC20Adapter.prototype, "contractAddress", {
        get: function () {
            return this._contractAddress;
        },
        enumerable: true,
        configurable: true
    });
    ERC20Adapter.prototype.generateRandomWallet = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.config.ethereumAdapter.generateRandomWallet()];
            });
        });
    };
    ERC20Adapter.prototype.importWalletWithSeed = function (seed) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.config.ethereumAdapter.importWalletWithSeed(seed)];
            });
        });
    };
    ERC20Adapter.prototype.importWallet = function (info) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.config.ethereumAdapter.importWallet(info)];
            });
        });
    };
    ERC20Adapter.prototype.getBalance = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = ethers_1.ethersBigNumberToBigNumber;
                        return [4 /*yield*/, this.contract.balanceOf(wallet.address)];
                    case 1: return [2 /*return*/, _a.apply(void 0, [_b.sent()])];
                }
            });
        });
    };
    ERC20Adapter.prototype.sendTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var ethWallet, sendOptions, _a, contract, transaction;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        ethWallet = ethers_1.ethersWallet(options.wallet.privateKey, this.config.ethereumAdapter.provider);
                        _a = {
                            gasLimit: GAS_LIMIT_FOR_TRANSACTION
                        };
                        return [4 /*yield*/, this.config.ethereumAdapter.provider.getGasPrice()];
                    case 1:
                        sendOptions = (_a.gasPrice = _b.sent(),
                            _a);
                        contract = this.contractFromWallet(options.wallet);
                        return [4 /*yield*/, contract.transfer(options.recipientAddress, ethers_1.toEthersAmount(options.amount), sendOptions)];
                    case 2:
                        transaction = _b.sent();
                        return [2 /*return*/, {
                                hash: transaction.hash,
                                date: new Date(),
                                fromAddress: transaction.from,
                                // Use the recipient address instead of transaction.to
                                // to have the actual recipient address instead of the smart contract address
                                toAddress: options.recipientAddress,
                                amount: options.amount,
                                crypto: this.crypto
                            }];
                }
            });
        });
    };
    ERC20Adapter.prototype.getTransactionFees = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var ethWallet, gasUsed, gasPrice, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        ethWallet = ethers_1.ethersWallet(params.wallet.privateKey, this.config.ethereumAdapter.provider);
                        gasUsed = AVERAGE_GAS_USED;
                        _a = ethers_1.ethersBigNumberToBigNumber;
                        return [4 /*yield*/, this.config.ethereumAdapter.provider.getGasPrice()];
                    case 1:
                        gasPrice = _a.apply(void 0, [_b.sent()]);
                        return [2 /*return*/, gasPrice.multipliedBy(gasUsed)];
                }
            });
        });
    };
    ERC20Adapter.prototype.getTransactions = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            var transactions, etherscanTransactions, _i, etherscanTransactions_1, transaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!wallet.address)
                            throw new Error('The provided wallet must have an address');
                        transactions = [];
                        return [4 /*yield*/, etherscan_1.getERC20Transactions(wallet.address, this.config.address)];
                    case 1:
                        etherscanTransactions = _a.sent();
                        for (_i = 0, etherscanTransactions_1 = etherscanTransactions; _i < etherscanTransactions_1.length; _i++) {
                            transaction = etherscanTransactions_1[_i];
                            transactions.push(etherscan_2.toTransaction(transaction, this.crypto));
                        }
                        return [2 /*return*/, transactions];
                }
            });
        });
    };
    ERC20Adapter.prototype.validateAddress = function (address, options) {
        return this.config.ethereumAdapter.validateAddress(address, options);
    };
    /**
     * Creates an ERC20 smart contract object to be able to perform transactions on behalf on the wallet
     * @param wallet The wallet that will perform transactions
     */
    ERC20Adapter.prototype.contractFromWallet = function (wallet) {
        var ethWallet = ethers_1.ethersWallet(wallet.privateKey, this.config.ethereumAdapter.provider);
        return new ethers.Contract(this.config.address, this._contractABI, ethWallet);
    };
    ERC20Adapter.prototype.setupContract = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, etherscan_1.getContractABI(this.config.address)];
                    case 1:
                        _a._contractABI = _f.sent();
                        this._contract = new ethers.Contract(this.config.address, this._contractABI, this.config.ethereumAdapter.provider);
                        _b = this;
                        return [4 /*yield*/, this.contract.symbol()];
                    case 2:
                        _b.crypto = _f.sent();
                        _c = this;
                        return [4 /*yield*/, this.contract.name()];
                    case 3:
                        _c.name = _f.sent();
                        _d = this;
                        _e = ethers_1.ethersBigNumberToBigNumber;
                        return [4 /*yield*/, this.contract.decimals()];
                    case 4:
                        _d.decimals = _e.apply(void 0, [_f.sent()]).toNumber();
                        this.setReady();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        wait_for_ready_1.WaitForReady()
    ], ERC20Adapter.prototype, "getBalance", null);
    __decorate([
        wait_for_ready_1.WaitForReady()
    ], ERC20Adapter.prototype, "sendTransaction", null);
    __decorate([
        wait_for_ready_1.WaitForReady()
    ], ERC20Adapter.prototype, "getTransactionFees", null);
    __decorate([
        wait_for_ready_1.WaitForReady()
    ], ERC20Adapter.prototype, "getTransactions", null);
    return ERC20Adapter;
}(wait_for_ready_1.AsyncObject));
exports.ERC20Adapter = ERC20Adapter;
