export declare const EOS = "0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0";
export declare const Tronix = "0xf230b790e05390fc8295f4d3f60332c93bed42e2";
export declare const Qtum = "0x9a642d6b3368ddc662CA244bAdf32cDA716005BC";
export declare const OmiseGO = "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07";
export declare const ICON = "0xb5a5f22694352c15b00323844ad545abb2b11028";
export declare const Binance = "0xB8c77482e45F1F44dE1745F52C74426C631bDD52";
export declare const Populous = "0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a";
export declare const Status = "0x744d70fdbe2ba4cf95131626614a1763df805b9e";
export declare const DigixDAO = "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a";
export declare const Maker = "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2";
export declare const Augur = "0xe94327d07fc17907b4db788e5adf2ed424addff6";
export declare const ZeroXProject = "0xe41d2489571d322189246dafa5ebde1f4699f498";
export declare const BasicAttentionToken = "0x0d8775f648430679a709e98d2b0cb6250d2887ef";
export declare const Qash = "0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6";
export declare const KyberNetwork = "0xdd974d5c2e2928dea5f71b9825b8b646686bd200";
export declare const Golem = "0xa74476443119A942dE498590Fe1f2454d7D4aC0d";
export declare const Ethos = "0x5af2be193a6abca9c8817001f45744777db30756";
export declare const FunFair = "0x419d0d8bdd9af5e606ae2232ed285aff190e711b";
export declare const Salt = "0x4156D3342D5c385a87D264F90653733592000581";
export declare const Request = "0x8f8221afbb33998d8584a2b05749ba73c37a938a";
export declare const NebulaAI = "0x17f8afb63dfcdcc90ebe6e84f060cc306a98257d";
export declare const TRX = "0xf230b790e05390fc8295f4d3f60332c93bed42e2";
export declare const QTUM = "0x9a642d6b3368ddc662CA244bAdf32cDA716005BC";
export declare const OMG = "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07";
export declare const ICX = "0xb5a5f22694352c15b00323844ad545abb2b11028";
export declare const BNB = "0xB8c77482e45F1F44dE1745F52C74426C631bDD52";
export declare const PPT = "0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a";
export declare const SNT = "0x744d70fdbe2ba4cf95131626614a1763df805b9e";
export declare const DGD = "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a";
export declare const MKR = "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2";
export declare const REP = "0xe94327d07fc17907b4db788e5adf2ed424addff6";
export declare const ZRX = "0xe41d2489571d322189246dafa5ebde1f4699f498";
export declare const BAT = "0x0d8775f648430679a709e98d2b0cb6250d2887ef";
export declare const QASH = "0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6";
export declare const KNC = "0xdd974d5c2e2928dea5f71b9825b8b646686bd200";
export declare const GNT = "0xa74476443119A942dE498590Fe1f2454d7D4aC0d";
export declare const ETHOS = "0x5af2be193a6abca9c8817001f45744777db30756";
export declare const FUN = "0x419d0d8bdd9af5e606ae2232ed285aff190e711b";
export declare const SALT = "0x4156D3342D5c385a87D264F90653733592000581";
export declare const REQ = "0x8f8221afbb33998d8584a2b05749ba73c37a938a";
export declare const NBAI = "0x17f8afb63dfcdcc90ebe6e84f060cc306a98257d";
