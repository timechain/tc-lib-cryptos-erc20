"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// By name
exports.EOS = '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0';
exports.Tronix = '0xf230b790e05390fc8295f4d3f60332c93bed42e2';
exports.Qtum = '0x9a642d6b3368ddc662CA244bAdf32cDA716005BC';
exports.OmiseGO = '0xd26114cd6EE289AccF82350c8d8487fedB8A0C07';
exports.ICON = '0xb5a5f22694352c15b00323844ad545abb2b11028';
exports.Binance = '0xB8c77482e45F1F44dE1745F52C74426C631bDD52';
exports.Populous = '0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a';
exports.Status = '0x744d70fdbe2ba4cf95131626614a1763df805b9e';
exports.DigixDAO = '0xe0b7927c4af23765cb51314a0e0521a9645f0e2a';
exports.Maker = '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2';
exports.Augur = '0xe94327d07fc17907b4db788e5adf2ed424addff6';
exports.ZeroXProject = '0xe41d2489571d322189246dafa5ebde1f4699f498';
exports.BasicAttentionToken = '0x0d8775f648430679a709e98d2b0cb6250d2887ef';
exports.Qash = '0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6';
exports.KyberNetwork = '0xdd974d5c2e2928dea5f71b9825b8b646686bd200';
exports.Golem = '0xa74476443119A942dE498590Fe1f2454d7D4aC0d';
exports.Ethos = '0x5af2be193a6abca9c8817001f45744777db30756';
exports.FunFair = '0x419d0d8bdd9af5e606ae2232ed285aff190e711b';
exports.Salt = '0x4156D3342D5c385a87D264F90653733592000581';
exports.Request = '0x8f8221afbb33998d8584a2b05749ba73c37a938a';
exports.NebulaAI = '0x17f8afb63dfcdcc90ebe6e84f060cc306a98257d';
// By symbol
exports.TRX = exports.Tronix;
exports.QTUM = exports.Qtum;
exports.OMG = exports.OmiseGO;
exports.ICX = exports.ICON;
exports.BNB = exports.Binance;
exports.PPT = exports.Populous;
exports.SNT = exports.Status;
exports.DGD = exports.DigixDAO;
exports.MKR = exports.Maker;
exports.REP = exports.Augur;
exports.ZRX = exports.ZeroXProject;
exports.BAT = exports.BasicAttentionToken;
exports.QASH = exports.Qash;
exports.KNC = exports.KyberNetwork;
exports.GNT = exports.Golem;
exports.ETHOS = exports.Ethos;
exports.FUN = exports.FunFair;
exports.SALT = exports.Salt;
exports.REQ = exports.Request;
exports.NBAI = exports.NebulaAI;
