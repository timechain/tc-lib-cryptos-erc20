import { EtherscanTransaction } from 'tc-lib-cryptos-ethereum/dist/vendor/etherscan';
export declare type ABI = any;
export declare function getContractABI(address: string): Promise<ABI>;
export declare function getERC20Transactions(address: string, smartContractAddress?: string): Promise<EtherscanTransaction[]>;
