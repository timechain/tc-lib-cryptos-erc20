"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function gweiToWei(gwei) {
    return gwei.multipliedBy(1000000000);
}
exports.gweiToWei = gweiToWei;
