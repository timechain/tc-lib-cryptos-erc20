"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var urlJoin = require("url-join");
function buildUrl(baseUrl) {
    var path = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        path[_i - 1] = arguments[_i];
    }
    if (arguments.length > 0) {
        var args = [baseUrl].concat(path);
        return urlJoin.apply(void 0, args);
    }
    return baseUrl;
}
exports.buildUrl = buildUrl;
function toQueryParams(params) {
    return Object.getOwnPropertyNames(params).map(function (key) {
        return key + "=" + params[key];
    }).join('&');
}
exports.toQueryParams = toQueryParams;
