import { BigNumber } from 'bignumber.js';
export declare function gweiToWei(gwei: BigNumber): BigNumber;
