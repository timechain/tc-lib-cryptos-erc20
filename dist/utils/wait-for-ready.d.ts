/**
 * Wait for init annotation.
 * Blocks the decorated function until the object is ready to use.
 * Will only work with async functions.
 */
export declare function WaitForReady(): (target: AsyncObject, name: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare class AsyncObject {
    private _isInitialized;
    private isInitCallbacks;
    readonly isInitialized: boolean;
    waitForInit(): Promise<void>;
    protected setReady(): void;
}
