import { EthersBigNumber, EthersTransaction } from 'tc-lib-cryptos-ethereum/dist/vendor/ethers';
export interface ERC20Contract {
    name(): Promise<string>;
    symbol(): Promise<string>;
    decimals(): Promise<EthersBigNumber>;
    balanceOf(address: string): Promise<EthersBigNumber>;
    transfer(toAddress: string, tokens: EthersBigNumber, options?: any): Promise<EthersTransaction>;
}
