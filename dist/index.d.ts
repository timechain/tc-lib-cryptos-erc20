import { BigNumber } from 'bignumber.js';
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models';
import { EthereumAdapterClass } from 'tc-lib-cryptos-ethereum';
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet';
import { ERC20Wallet } from './models/wallet';
import { ERC20Contract } from './models/erc20-contract';
import { AsyncObject } from './utils/wait-for-ready';
import * as contractsAddresses from './constants/contracts-addresses';
/**
 * Non exhaustive list of ERC20 smart contracts addresses
 */
export declare const ContractsAddresses: typeof contractsAddresses;
export declare class ERC20Adapter extends AsyncObject implements WalletAdapter<ERC20Wallet> {
    private config;
    crypto: string;
    name: string;
    decimals: number;
    private _contractAddress;
    private _contract;
    private _contractABI;
    readonly contract: ERC20Contract;
    readonly contractAddress: string;
    constructor(config: {
        /**
         * The smart contract address
         */
        address: string;
        /**
         * The Ethereum adapter to use to perform the ERC20 operations
         */
        ethereumAdapter: EthereumAdapterClass;
    });
    generateRandomWallet(): Promise<{
        wallet: ERC20Wallet;
        seed: string;
    }>;
    importWalletWithSeed(seed: string): Promise<ERC20Wallet>;
    importWallet(info: {
        publicKey?: string;
        privateKey?: string;
        address?: string;
    }): Promise<ERC20Wallet>;
    getBalance(wallet: Partial<Wallet>): Promise<BigNumber>;
    sendTransaction(options: SendTransactionOptions<ERC20Wallet>): Promise<Transaction>;
    getTransactionFees(params: SendTransactionOptions<ERC20Wallet>): Promise<BigNumber>;
    getTransactions(wallet: Partial<Wallet>): Promise<Transaction[]>;
    validateAddress(address: string, options: object | null): boolean;
    /**
     * Creates an ERC20 smart contract object to be able to perform transactions on behalf on the wallet
     * @param wallet The wallet that will perform transactions
     */
    private contractFromWallet(wallet);
    private setupContract();
}
